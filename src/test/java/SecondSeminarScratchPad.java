import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SecondSeminarScratchPad {

//             q |~~
//            /-\|
//          ["^\ | |_
//            \ ||_' )~
//            ||_| ||
//            ||   ||

    @Test
    public void whichWeaponsAKnightShouldTakeWithHim() throws Exception {
        final String NECESSARY_WEAPON_ATTRIBUTE = "Steel";

        Stream<String> availableWeapons = Stream.of("Huge Steel Armor",
                "Heavy Wooden Sword",
                "Uncomfortable Leather Boots",
                "Protective Steel Shoes",
                "Steel-enhanced Shield",
                "Flimsy Shield");

        // HINT: Use the Stream above and filter the results
        // HINT: To get a list of items, use Collectors.toList()
        final List<String> goodWeaponsToTake = null;

        assertEquals(3, goodWeaponsToTake.size());
    }

    @Test
    public void aKnightNeedsToKnowHowLongItTakesToTravelToWhereAPrincessIs() {
        final int TOTAL_DAYS = 10;

        int DAYS_TRAVEL_TO_TOWN_A = 3;
        int DAYS_TRAVEL_BETWEEN_TOWNS_A_B = 5;
        int DAYS_TRAVEL_BETWEEN_TOWNS_B_C = 2;

        List<Integer> travelDays = Arrays.asList(DAYS_TRAVEL_TO_TOWN_A,
                DAYS_TRAVEL_BETWEEN_TOWNS_A_B,
                DAYS_TRAVEL_BETWEEN_TOWNS_B_C);

        // HINT: use Stream and then convert to IntStream to get the sum
        // HINT: or use Stream and then "reduce" function
        final Integer sumOfDays = null;

        // then
        assertEquals(TOTAL_DAYS, sumOfDays.intValue());
    }

    @Test
    public void illustrateFlatMap() {

        List<Integer> ints = new ArrayList<>();

//        ints.add(Arrays.asList(1, 2, 3));
        ints.addAll(Arrays.asList(4, 5, 6, 7, 8, 9));
//        ints.add(Arrays.asList(10, 11, 12, 13));
//        ints.add(Arrays.asList(14));

        ints.stream().flatMap(i -> IntStream.range(0, i).boxed()).forEach(System.out::println);

    }

    @Test
    public void filterStreamIllustratingIntermediateAndTerminalOperations() {

        Stream<Integer> ints = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

//        System.out.println(ints.filter(i -> i % 2 == 0).peek(System.out::println));
        System.out.println(ints.filter(i -> i % 2 == 0).peek(System.out::println).distinct());

    }

    @Test
    public void sieveOfErastothenese() {

        // compute primes between 2 and 100
        /**
         * Create a list of consecutive integers from 2 through n: (2, 3, 4, ..., n).
         Initially, let p equal 2, the first prime number.
         Starting from p, enumerate its multiples by counting to n in increments of p, and mark them in the list (these will be 2p, 3p, 4p, ..., n ;
         the p itself should not be marked).                                                                                    xp = 100, x = 100 / p = 50
         Find the first number greater than p in the list that is not marked. If there was no such number, stop. Otherwise,
         let p now equal this new number (which is the next prime), and repeat from step 3.
         When the algorithm terminates, the numbers remaining not marked in the list are all the primes below n.
         */

        int p = 2;
        final int n = 10;
        boolean done = false;

        List<Integer> computedPrimes = new ArrayList<>();
        computedPrimes.add(p);

        // create markable integer instances from p to n sequence
        List<Integer> sequenceFromPToN = IntStream.rangeClosed(p, n).boxed().collect(toList());
        List<MarkableInteger> ints = sequenceFromPToN.stream().map(MarkableInteger::new).collect(toList());

        while(!done) {

            final Integer currentPrime = p;

            ints.stream()
                    .filter(i -> i.value > currentPrime)
                    .filter(i -> i.value % currentPrime == 0)
                    .forEach(i -> i.isMarked = true);

            Optional<MarkableInteger> nextPrime = ints.stream().filter(i -> i.value > currentPrime)
                                                               .filter(i -> !i.isMarked)
                                                               .findFirst();

            if (nextPrime.isPresent()) {

                System.out.println(nextPrime.get().value);
                computedPrimes.add(nextPrime.get().value);
                p = nextPrime.get().value;

            } else {
                done = true;
                System.out.println("we have computed all the primes");
            }
        }

        Integer[] expectedPrimes = {2, 3, 5, 7};
        Integer[] actualPrimes = computedPrimes.toArray(new Integer[computedPrimes.size()]);




        assertArrayEquals("expected these primes when n = 10", expectedPrimes, actualPrimes);
    }




    class MarkableInteger {
        int value;
        boolean isMarked;

        MarkableInteger(int value) {
            this.value = value;
            this.isMarked = false;
        }
    }

}
