import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class Streams {

    @Test
    public void _1_creatingAStreamFromAnArray() {
        final Country[] countries = (Country[]) COUNTRIES.toArray();

        // TODO - create a stream from an array : (hint: "of")
        final Stream<Country> stream = Stream.empty();

        final List<Country> expected = stream.collect(toList());
        assertThat(expected, equalTo(COUNTRIES));
    }

    @Test
    public void _2_creatingAStreamFromACollection() {
        // TODO - create a stream from a collection : (hint: "stream")
        final Stream<Country> stream = Stream.empty();

        final List<Country> expected = stream.collect(toList());
        assertThat(expected, equalTo(COUNTRIES));
    }

    @Test
    public void _3_filteringAStream() {
        // TODO - filter stream for countries with English as language : (hint: "filter")
        final Stream<Country> stream = COUNTRIES.stream();
        assertThat(stream.count(), equalTo(3L));
    }

    @Test
    public void _4_limitingResultsFromAStream() {
        // TODO - filter for countries that speak Spanish, but only return the first two : (hint : "limit")
        final Stream<Country> stream = COUNTRIES.stream().filter(c -> c.language.equals("Spanish"));
        assertThat(stream.count(), equalTo(2L));
    }

    @Test
    public void _5_mappingAStream() {
        // TODO - we only want the names of the countries : (hint : "map")
        final List<String> EXPECTED_COUNTRY_NAMES = Arrays.asList("USA", "Brazil", "Japan", "Canada", "Ecuador",
                                                    "Argentina", "Australia", "Spain", "Costa Rica", "Russia");

        // TODO - map a Country stream to a String stream with the names of the countries : (hint : "map")
        final List<String> ACTUAL_COUNTRY_NAMES = Collections.EMPTY_LIST;

        assertThat(EXPECTED_COUNTRY_NAMES, equalTo(ACTUAL_COUNTRY_NAMES));
    }

    @Test
    public void _6_filteringUniqueElements() {
        final List<String> EXPECTED_UNIQUE_LANGUAGES = Arrays.asList("English", "Spanish", "Portuguese", "Japanese");

        // TODO - map to a stream of String with the languages, and then find the unique languages : (hint : "distinct")
        final List<String> ACTUAL_UNIQUE_LANGUAGES = Collections.emptyList();

        assertThat(ACTUAL_UNIQUE_LANGUAGES, equalTo(EXPECTED_UNIQUE_LANGUAGES));
    }

    /******************  Don't touch this code ****************/
    class Country {
        String name;
        String language;
        Country(String name, String language) {
            this.name = name;
            this.language = language;
        }
        @Override
        public String toString() {
            return name + '[' + language + ']';
        }
    }

    private final List<Country> COUNTRIES = Arrays.asList(
            (new Country("USA", "English")),
            (new Country("Brazil", "Portuguese")),
            (new Country("Japan", "Japanese")),
            (new Country("Canada", "English")),
            (new Country("Ecuador", "Spanish")),
            (new Country("Argentina", "Spanish")),
            (new Country("Australia", "English")),
            (new Country("Spain", "Spanish")),
            (new Country("Costa Rica", "Spanish")),
            (new Country("Portugal", "Portuguese"))
    );
}
