package conversant.exercises;

import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class _1_LambdaVsAnonymousClassExercise {

    private Object buttonClicked;

    @Before
    public void setUp() {
        buttonClicked = null;
    }

    @Test
    public void testUsingAnonymousClass() {
        JButton buttonWithAnonymousClassForActionListener = new JButton("Button 1");
        buttonWithAnonymousClassForActionListener.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonClicked = e.getSource();
            }
        });
        buttonWithAnonymousClassForActionListener.doClick();
        assertThat("expected the button with the anonymous class to be clicked", buttonClicked, equalTo(buttonWithAnonymousClassForActionListener));
    }

    @Test
    public void testUsingLambda() {
        JButton buttonWithLambaExpressionActionListener = new JButton("Button 2");
        buttonWithLambaExpressionActionListener.doClick();

        // TODO - implement using a lambda
        // TODO HINT - the format of a lambda is
        //
        // (SIGNATURE) -> BODY
        // e.g.
        // (TypeClass arg) -> BODY
        //
        ActionListener lambdaExpressionActionListener = e -> buttonClicked = e.getSource();

        buttonWithLambaExpressionActionListener.addActionListener(lambdaExpressionActionListener);
        buttonWithLambaExpressionActionListener.doClick();

        assertThat("expected the button with the lambda expression to be clicked", buttonClicked, equalTo(buttonWithLambaExpressionActionListener));
    }

}
