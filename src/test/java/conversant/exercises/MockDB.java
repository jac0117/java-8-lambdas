package conversant.exercises;

import java.util.LinkedList;

public class MockDB<T> implements DB<T> {
    private LinkedList<T> consumedCards;

    public MockDB(LinkedList<T> consumedCards) {

        this.consumedCards = consumedCards;
    }

    public void save(T saveIt) {
      consumedCards.add(saveIt);
    }
}
