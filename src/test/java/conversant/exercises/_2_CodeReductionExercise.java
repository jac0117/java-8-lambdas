package conversant.exercises;

import conversant.Ad;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static conversant.AdType.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class _2_CodeReductionExercise {

    private final List<Ad> adCollection = Arrays.asList(
            new Ad(BANNER, "Back to school promotions"),
            new Ad(SMART, "Summer promotions"),
            new Ad(SMART, "Conversant generated smart ad"),
            new Ad(BANNER, "School closing"),
            new Ad(TEXT, "Work savings"),
            new Ad(TEXT, "School savings"),
            new Ad(BANNER, "Competing network ad"),
            new Ad(BANNER, "Small"),
            new Ad(TEXT, "School teachers saving day"),
            new Ad(BANNER, "Back to school final sale")
    );

    /** Not reusable, composable, or decomposable **/

    private List<Ad> getBannerAds(List<Ad> ads) {
        List<Ad> filteredAds = new ArrayList<>();
        for(Ad ad : ads) {
            if(ad.type.equals(BANNER))
                filteredAds.add(ad);
        }
        return filteredAds;
    }

    private List<Ad> getTextAds(List<Ad> ads) {
        List<Ad> filteredAds = new ArrayList<>();
        for(Ad ad : ads) {
            if(ad.type.equals(TEXT))
                filteredAds.add(ad);
        }
        return filteredAds;
    }

    private List<Ad> getAdsWithLongNames(List<Ad> ads) {
        List<Ad> filteredAds = new ArrayList<>();
        for(Ad ad : ads) {
            if(ad.name.length() > 20)
                filteredAds.add(ad);
        }
        return filteredAds;
    }

    private List<Ad> getAdsWithShortNames(List<Ad> ads) {
        List<Ad> filteredAds = new ArrayList<>();
        for(Ad ad : ads) {
            if(ad.name.length() < 7)
                filteredAds.add(ad);
        }
        return filteredAds;
    }



    @Test
    public void testGetAdsWithOverheadCode() {
        assertThat(getBannerAds(adCollection).size(), equalTo(5));
        assertThat(getTextAds(adCollection).size(), equalTo(3));
        assertThat(getAdsWithLongNames(adCollection).size(), equalTo(4));
        assertThat(getAdsWithShortNames(adCollection).size(), equalTo(1));
    }

    /** ----------------------------------------------------------------------------------- **/

    @Test
    public void testGetAdsWithLessCodeUsingLambdas() {

        // TODO - implement java.util.function.Predicate<Ad> lambdas to filter the ads

        Predicate<Ad> replaceThisPredicateWithOneThatWorks = ad -> false; // obviously this predicate won't work!

        assertThat("expected to find 5 BANNER ads", filterWithLambdas(adCollection, replaceThisPredicateWithOneThatWorks).size(), equalTo(5));
        assertThat("expected to find 3 TEXT ads", filterWithLambdas(adCollection, replaceThisPredicateWithOneThatWorks).size(), equalTo(3)); //
        assertThat("expected to find 4 ads with names longer than 20 characters", filterWithLambdas(adCollection, replaceThisPredicateWithOneThatWorks).size(), equalTo(4));
        assertThat("expected to find 1 ad with name shorter than 7 characters", filterWithLambdas(adCollection, replaceThisPredicateWithOneThatWorks).size(), equalTo(1));
    }

    private List<Ad> filterWithLambdas(List<Ad> ads, Predicate predicate) {
        List<Ad> filteredAds = new ArrayList<>();
        for (Ad ad : ads) {
            if(predicate.test(ad))
                filteredAds.add(ad);
        }
        return filteredAds;
    }
}
