package conversant.exercises;

import conversant.Ad;
import conversant.AdType;
import org.junit.Test;

import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class _4_FunctionalInterfaceExercise {


    @Test
    public void consumersUsuallyAreForSideEffectingProcedures() throws Exception {

        LinkedList<Ad> ads = new LinkedList<>();
        DB db = new MockDB(ads);


        Consumer<Ad> dbLogger = new Consumer<Ad>() {
            @Override
            public void accept(Ad ad) {
                // use db to save an ad
            }
        };



        Ad savedAd = new Ad(AdType.BANNER, "Test Ad");
        dbLogger.accept(savedAd);
        assertThat(ads.peek(), is(savedAd));

    }

    @Test
    public void suppliersAreAGreatWayToDelayEvaluation() {
        // delay eval of really expensive operations
        String adName = adFilter(AdType.TEXT, new Supplier<Ad>() {
            @Override
            public Ad get() {
                return null;
            }
        });
        assertThat(adName, is("i am smart ad"));
    }

    private String adFilter(AdType type, Supplier<Ad> fn) {
        if(type == AdType.BANNER)
            return fn.get().name;
        return "No AD for You!";
    }

}
