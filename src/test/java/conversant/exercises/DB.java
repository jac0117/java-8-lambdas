package conversant.exercises;

@FunctionalInterface
public interface DB<T> {
    void save(T saveIt);
}
