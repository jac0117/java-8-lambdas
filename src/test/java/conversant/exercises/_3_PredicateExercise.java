package conversant.exercises;

import conversant.Ad;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static conversant.AdType.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class _3_PredicateExercise {

    private List<Ad> adCollection = null;
    private Predicate<Ad> REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS = ad -> false;

    @Before
    public void setUp() {
        adCollection = Arrays.asList(
                new Ad(BANNER, "BACK TO SCHOOL!!"),
                new Ad(SMART, "back to school"),
                new Ad(SMART, "some other ad"),
                new Ad(BANNER, "school's out!"),
                new Ad(TEXT, "back to work"),
                new Ad(TEXT, "Back to School"),
                new Ad(BANNER, "not this ad"),
                new Ad(BANNER, "this one instead"),
                new Ad(TEXT, "I love school"),
                new Ad(BANNER, "Hey!  Get back to school!")
        );
    }

    @Test
    public void getAllTheAdsOfTypeBanner() {

        final List bannerAds = filter(adCollection, REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS);

        assertThat("Expected to find 5 BANNER ads", bannerAds.size(), equalTo(5));
    }

    @Test
    public void getsAllAdsThatAreEitherOfTypeTextOrBanner() {

        final List textAds = filter(adCollection, REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS);

        assertThat("Expected to find 3 TEXT ads", textAds.size(), equalTo(3));
    }

    @Test
    public void getsAllAdsThatAreEitherOfTypeTextOrBannerThroughPredicateComposition() {
        //1. Create a predicate value here that finds Banners
        Predicate<Ad> banner = REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS;
        //2. Creates another predicate value that finds Text
        Predicate<Ad> text = REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS;
        //What 'function' might 'glue' these two predicates together? What's the result?
        Predicate<Ad> eitherOr = REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS;
        // plug in your 'composition' below
        final List bannerOrTextAds = filter(adCollection, eitherOr);

        assertThat("Expected to find 8 BANNER/TEXT ads", bannerOrTextAds.size(), equalTo(8));
    }

    @Test
    public void getAllTheAdsThatAreNotOfTypeBannerByPredicateNegation() {

        Predicate<Ad> banner = REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS;

        Predicate<Ad> notBanner = REPLACE_THIS_LAMBDA_WITH_ONE_THAT_WORKS;
        final List notBannerAds = filter(adCollection, notBanner);

        assertThat("expected to find 5 ads not of type BANNER", notBannerAds.size(), equalTo(5));

    }


    private List<Ad> filter(List<Ad> ads, Predicate<Ad> predicate) {
        final List<Ad> filteredAds = new ArrayList<>();
        for(Ad ad : ads) {
            if(predicate.test(ad)) {
                filteredAds.add(ad);
            }
        }
        return filteredAds;
    }

}
