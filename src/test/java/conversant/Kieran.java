package conversant;

import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class Kieran {

    public static Predicate<String> checkIfStartsWith(final String letter) {
        return str -> str.startsWith(letter);
    }

    public static void main(String[] args) {

        final List<String> names = Arrays.asList("Jo", "Kieran", "Janile", "Salty", "Pauline", "Omar", "Charles");
        final List<String> emptyNames = new ArrayList<String>();

        System.out.println("External Iteration:");
        for (String name : names) {
            System.out.println(name);
        }
        System.out.println();

        System.out.println("Using forEach with lambda:");
        names.forEach(name -> System.out.println(name));

        System.out.println();

        System.out.println("Using forEach with method reference:");
        names.forEach(System.out::println);

        System.out.println();


        List<Integer> integers = new ArrayList<Integer>();

        for(int i  = 1; i <= 10; i++) {
            integers.add(i);
        }

        List<String> plusTen = integers.stream()
                                        .map(i -> i + 10)
                                        .map(i -> Integer.valueOf(i).toString())
                                        .collect(Collectors.toList());

        plusTen.forEach(System.out::println);

        System.out.println();


        final String letterForStaticMethodInvocation = "K";
        System.out.println(names.stream().filter(checkIfStartsWith(letterForStaticMethodInvocation)).findFirst().get());

        System.out.println();

        final Function<String, Predicate<String>> startsWithLetter =
                (String letter) -> {
                    Predicate<String> checkStarts = (String name) -> name.startsWith(letter);
                    return checkStarts;
                };


        Optional<String> maybeName =  names.stream()
                .filter(startsWithLetter.apply("F+"))
                .findFirst();

        maybeName.ifPresent(System.out::println);

        System.out.println(maybeName.orElse("No name found"));

        System.out.println();

        int sumLengthsOfNames = names.stream()
                .mapToInt(name -> name.length())
                .sum();

        System.out.println(sumLengthsOfNames);

        System.out.println();

        final Optional<String> longestName = names.stream()
                .reduce((a, b) -> a.length() > b.length() ? a : b);

        System.out.println("longest name: " + longestName.orElse("none found"));

        System.out.println();

        String allNames = names.stream().collect(joining(", "));

        System.out.println(allNames);

        System.out.println();


        boolean eval = Evaluation.evaluate(19);
        System.out.println(eval);

        Evaluation.eagerEvaluator(Evaluation.evaluate(111), Evaluation.evaluate(123));

        System.out.println();

        Supplier<Boolean> inputOne = () -> Evaluation.evaluate(1);
        Supplier<Boolean> inputTwo = () -> Evaluation.evaluate(123);

        Evaluation.lazyEvaluator(inputOne, inputTwo);

        System.out.println();

        String firstNameLongerThanThreeCharacters =
                names.stream()
                .filter(name -> length(name) > 3)
                .map(name -> upper(name))
                .findFirst().orElse("none found");

        System.out.println(firstNameLongerThanThreeCharacters);

        System.out.println();

        Primes.primes(100, 10000).stream().forEach(System.out::println);

        Supplier<String> thisThatTheOtherThing = () -> "this" + "that" + "the other thing";

        Supplier<String> anonymousSupplier = new Supplier<String>() {
            @Override
            public String get() {
                return "this" + "that" + "the other thing";
            }
        };


        System.out.println(thisThatTheOtherThing.get());
        System.out.println(anonymousSupplier.get());

        System.out.println();

        String reduced = names.stream()
                .reduce("", (a, b) -> a.concat(b));

        System.out.println(reduced);


        System.out.println();


        System.out.println();
        System.out.println("done");
    }




    static int length(String str) {
        System.out.println("checking length of " + str);
        return str.length();
    }

    static String upper(String str) {
        System.out.println("converting to upper case: " + str);
        return str.toUpperCase();
    }

    static class Evaluation {
        public static boolean evaluate(final int value) {
            System.out.println("evaluating " + value + "...");
            return value > 100;
        }

        public static void eagerEvaluator(final boolean inputOne, final boolean inputTwo) {
            System.out.println("eager evaluator called...");
            System.out.println("accept?: " + (inputOne && inputTwo));
        }

        public static void lazyEvaluator(final Supplier<Boolean> inputOne, final Supplier<Boolean> inputTwo) {

            System.out.println("lazy evaluator called...");
            System.out.println("accept?: " + (inputOne.get() && inputTwo.get()));
        }
    }

    static class Primes {

        public static boolean isPrime(final int number) {
            return number > 1 &&
                    IntStream.rangeClosed(2, (int)Math.sqrt(number))
                            .noneMatch(divisor -> number % divisor == 0);
        }

        public static int primeAfter(final int n) {
            if(isPrime(n + 1)) {
                return n + 1;
            }
            return primeAfter(n + 1);
        }

        public static List<Integer> primes(final int from, final int count) {
            return Stream.iterate(primeAfter(from - 1), Primes::primeAfter)
                    .limit(count)
                    .collect(Collectors.<Integer>toList());
        }
    }
}
