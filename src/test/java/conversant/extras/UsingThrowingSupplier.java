package conversant.extras;

public class UsingThrowingSupplier {
    public static <T>T withThrowing(ThrowingSupplier<T> fn){
        try{
            return fn.get();
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
}
