package conversant.extras;

import com.google.common.collect.ImmutableList;
import conversant.Ad;
import conversant.AdType;
import org.junit.Test;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static junit.framework.Assert.assertEquals;

public class LambdaBasicsTest {




    public void functionsInFullFormat() {
        Function<String, Integer> takesAStringReturnsInteger = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return 1;
            }
        };

        Predicate<String> takesAStringReturnsBoolean  = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return false;
            }
        };

        Consumer<String> consumerThatTakesAString = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println("s is => " + s);
            }
        };

        Supplier<String> supplierThatReturnsAString = new Supplier<String>() {
            @Override
            public String get() {
                return "Hello";
            }
        };
    }

    public void functionsAsLambdas() {
        Function<String, Integer> takesAStringReturnsInteger = s -> 1;

        Predicate<String> takesAStringReturnsBoolean  = s -> false;

        Consumer<String> consumerThatTakesAString = s -> System.out.println("s is => " + s);

        Supplier<String> supplierThatReturnsAString = () -> "Hello";
    }

    // accepting functions as arguments

    @Test
    public void getFirstBannerAdTest() {
        Ad thisIsTheAdYouAreLookingFor = new Ad(AdType.BANNER, "Banner1");

        List<Ad> ads = ImmutableList.of(new Ad(AdType.TEXT, "Text1"), new Ad(AdType.TEXT, "Text2"),
                thisIsTheAdYouAreLookingFor, new Ad(AdType.TEXT, "Text3"));

        assertEquals( thisIsTheAdYouAreLookingFor.name , getFirstBannerAd(ads).name);
    }

    private Ad getFirstBannerAd(List<Ad> ads) {
        for(Ad a : ads) {
            if(AdType.BANNER.equals(a.type)) {
                return a;
            }
        }
        throw new RuntimeException("Is not finding an Ad really 'exceptional'?");
    }


    @Test
    public void getFirstTextAd_WhoseNameEndsWith_Three() {

        List<Ad> ads = ImmutableList.of(new Ad(AdType.TEXT, "Text1"), new Ad(AdType.TEXT, "Text2"),
                new Ad(AdType.BANNER, "Banner1"), new Ad(AdType.TEXT, "Text3"), new Ad(AdType.TEXT, "Text4"));

        assertEquals( "Text3" , getFirstTextAdWhoseNameEndsWithDigitThree(ads).name);
    }

    //start feeling the pain .... duplicate code.... is there an easier way?
    private Ad getFirstTextAdWhoseNameEndsWithDigitThree(List<Ad> ads) {
        for(Ad a : ads) {
            if(AdType.TEXT.equals(a.type) && a.name.endsWith("3")) {
                return a;
            }
        }
        throw new RuntimeException("Is not finding an Ad really 'exceptional'?");
    }


    @Test
    public void returnFirstMatchingAd() {
        List<Ad> ads = ImmutableList.of(new Ad(AdType.SMART, "Smart1"), new Ad(AdType.TEXT, "Text1"), new Ad(AdType.TEXT, "Text2"),
                new Ad(AdType.BANNER, "Banner1"), new Ad(AdType.TEXT, "Text3"), new Ad(AdType.TEXT, "Text4"));
        
        assertEquals("Banner1", getFirstIn((ad -> ad.type.equals(AdType.BANNER)), ads).name);
        assertEquals("Text3",   getFirstIn((ad -> ad.type.equals(AdType.TEXT) && ad.name.endsWith("3")), ads).name);
        // get the first smart ad
    }

    private Ad getFirstIn(Predicate<Ad> p, List<Ad> ads) {
        for(Ad a : ads) {
            if(p.test(a)) {
                return a;
            }
        }
        throw new RuntimeException("Is not finding an Ad  really 'exceptional'?"); //Optional!?!
    }

    // returning functions from functions


}
