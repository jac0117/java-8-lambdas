package conversant.extras;

import conversant.*;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class HigherOrderedFunctionsTest {

    @Test
    public void higherOrderedFunctionsAllowFunctionsAsParams() throws Exception {

        Function<String, String> greetFn = s -> String.format("Greetings %s," , s);

        String result = andSalutations("Bill",greetFn);

        assertEquals("Greetings Bill, and Salutations!", result);
    }

    private String andSalutations(String s , Function<String, String> fn) {
        return fn.apply(s) + " and Salutations!";
    }


    @Test
    public void higherOrderedFunctionsAllowFunctionsAsReturnValues() throws Exception {

        Function<String, String> greetFn = s -> String.format("Greetings %s," , s);

        Function<String, String> greeter = getGreeter(greetFn);

        assertEquals("Greetings Bill, and Salutations!", greeter.apply("Bill"));
    }

    private Function<String, String> getGreeter(Function<String, String> fn) {
        return s -> fn.apply(s) + " and Salutations!";
    }

    @Test
    public void havingHigherOrderedFunctions_AllowFor_FunctionComposition() throws Exception {

        Function<String, String> greeetings = s -> String.format("Greetings %s," , s);
        Function<String, String> salutations = s -> s + " and Salutations!";

        final Function<String, String> composed = greeetings.andThen(salutations);

        assertEquals("Greetings Bill, and Salutations!", composed.apply("Bill"));
    }

    @Test
    public void weCanEvenComposeThings_Programmatically() throws Exception {

        Function<String, String> greetings = s -> String.format("Greetings %s! " , s);
        Function<String, String> salutations = s -> s + " .... and Salutations!";
        Function<String, String> insults     = s -> s + " Tu Quoque! ";

        // True "reuse"
        Function<String, Function<String, String>> confab = functionWeaver(greetings, salutations, insults);

        assertEquals("Bill Go to blazes!",
                confab.apply("Bill").apply("You Debbill!"));

        final Function<String, String> floweryGreeting = confab.apply("Mr T."); // just plugin To: and ....

        final String nowWrapUpTheJibbaJabba = floweryGreeting.apply("to you");

        assertEquals("Ah Mr T.! Blessings Be upon thee! Greetings to you!  .... and Salutations!",nowWrapUpTheJibbaJabba);
    }

    private  Function<String,Function<String,String>> functionWeaver(
            Function<String, String> fn1,
            Function<String, String> fn2,
            Function<String, String> fn3) {
        return name -> {
            if("Bill".equals(name))
                return fn1.andThen(fn3).andThen(s -> name  + " Go to blazes!" );
            return fn1.andThen(fn2).andThen((String s)-> "Ah " + name  + "! Blessings Be upon thee! " + s);

        };
    }


    @Test
    public void curriedFunctions() throws Exception {

        Function<Integer, Function<Integer, Function<Integer, Integer>>>
                curried = a -> b -> c -> a + b + c;

        final Integer apply = curried.apply(1).apply(2).apply(3);
        // why is this interesting ... `punting`
        assertEquals(apply.intValue(), 6);
    }

    @Test
    public void closingOverValues() throws Exception {

        Supplier<Long> ginc = Closures.getThenIncrement(1L);
        assertEquals((Long) 1L, ginc.get());
        assertEquals((Long) 2L, ginc.get());
        assertEquals((Long) 3L, ginc.get());

        Supplier<Long> incg = Closures.incrementThenGet(1L);
        assertEquals((Long) 2L, incg.get());
        assertEquals((Long) 3L, incg.get());
        assertEquals((Long) 4L, incg.get());

        Function<Long,Long> adder =   Closures.makeAdder(5L);
        assertEquals((Long) 8L, adder.apply(3L));
        assertEquals((Long) 15L, adder.apply(10L));
    }

    @Test
    public void passingFunctions_CanHelpUs_ReduceIncidentalDependencies() throws Exception {

        final AdDBService db = new AdDBService();

        Function<AdType, PriceyAd> doesDbCall = (AdType type) -> db.fetchFor(type);
        final MyAdCostService myAdCostService = new MyAdCostService(doesDbCall);
        Money price  = myAdCostService.costFof(AdType.BANNER);

        assertEquals(new Money(20, "USD"), price);

    }

    @Test
    public void passingFunctions_HelpsUs_Eliminate_Context() throws Exception {

        final SuperDuperExpensiveDBService db = new SuperDuperExpensiveDBService();

        // im not going to expose the service to "how" the call to the db works.
        //  like injection no? but you don't have to roll your own class/wrapper.
        Function<AdType,PriceyAd> doesDbCall = (AdType type) -> db.fetchFor(type);


        // what does the service depend on Ad and Ad Type ... no db ... no "helper" class
        // how 'easy' is that to test?
        // don't have to come up with some 'name' for the idea -
        //      its signature tells a sufficient whole story.
        //      reduces the 'kingdom of nouns'
        final MyAdCostService adService = new MyAdCostService(doesDbCall);

        Money bannerCost  = adService.costFof(AdType.BANNER);
        assertEquals(new Money(20, "USD"), bannerCost);

        Money textAdCost  = adService.costFof(AdType.TEXT);
        assertEquals(new Money(10, "USD"), textAdCost);

    }

    @Test public void context_vs_composition(){
        Function<Long,Long> adder =   Closures.makeAdder(5L);

        final Function<Long, Double> composedFn = adder
                .andThen(i -> i * 10)
                .andThen(i -> Math.pow(i, 10.0));

        final Double result = composedFn.apply(15L);

        assertEquals( Math.pow( (5+ 15) * 10 , 10.0) , result.doubleValue(), 0.0D);

        // note that the expected ->> needs <<-- all the values to do the calculation in one context
        // the composed result does not, in fact you can reuse adder.
        //   no computation takes place until the last `apply`
        //      - you are building a `data-structure` that may do `computation`
        //
    }

    @Test public void reimplementingTryWithResources() throws IOException {

        ThrowingSupplier<Stream<String>> stream = () -> {
            Path path = Paths.get("./src/test/resources", "stuff.txt");
            return Files.lines(path);
        };

        final Optional<String> result = new Try<String>().withResources(stream).apply(s -> s.findFirst());
        assertTrue(result.get().endsWith("It works!"));


        final Optional<String> result_alternate = new Try<String>().withResources(stream, s -> s.findFirst());
        assertTrue(result_alternate.get().endsWith("It works!"));

    }
}



