package conversant.extras;

@FunctionalInterface
public interface ThrowingSupplier<T>{
    T get() throws Exception;
}
