package conversant.extras;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class Try<T> {
    public   Optional<T> 
        withResources(
            ThrowingSupplier<Stream<T>> using, 
            Function<Stream<T>, Optional<T>> fn) {

        final Stream<T> stream = getTheStream(using);
        try {
            return fn.apply(stream);
        }catch (Exception e) {
            throw new RuntimeException("boom");
        }finally {
            stream.close();
        }

    }

    public Function<Function<Stream<T>, Optional<T>>, Optional<T>> 
        withResources(ThrowingSupplier<Stream<T>> using) {

        final Stream<T> stream = getTheStream(using);
        try {
            return fn -> {
                final Optional<T> apply = fn.apply(stream);
                stream.close();
                return apply;
            };
        }catch (Exception e) {
            return oops -> {
                throw new RuntimeException("boom");
            };
        }
    }

    private Stream<T> getTheStream(ThrowingSupplier<Stream<T>> closeable) {
        try {
            return closeable.get();
        } catch (Exception e) {
            return null;
        }
    }
}
