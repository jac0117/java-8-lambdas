package conversant.extras;


import conversant.AdType;

import java.util.function.Function;


public class MyAdCostService {

    private final Function<AdType, PriceyAd> adLocator;

    public MyAdCostService(Function<AdType, PriceyAd> adLocator) {
        this.adLocator = adLocator;
    }

    public Money costFof(AdType type) {
        Money m = Money.DE_NADA;
        final PriceyAd ad = adLocator.apply(type);
        switch (type){
            case BANNER:
                m = new Money(10, "USD");
                break;
            case SMART:
                m = new Money(20,"USD");
                break;
        }
        return Money.plus(m, ad.price());
    }

}
