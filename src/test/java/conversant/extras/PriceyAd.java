package conversant.extras;

import conversant.AdType;

import java.util.Objects;

public class PriceyAd {

    public AdType type;
    public String name;

    public PriceyAd(AdType type, String name) {
        this.type = type;
        this.name = name;
    }

    public Money price() {
        return new Money(10,"USD");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriceyAd ad = (PriceyAd) o;
        return Objects.equals(type, ad.type) &&
                Objects.equals(name, ad.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }
}
