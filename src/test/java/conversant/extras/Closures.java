package conversant.extras;

import java.util.function.Function;
import java.util.function.Supplier;

public class Closures {
    public static Supplier<Long> getThenIncrement(Long seed) {
        return new Supplier<Long>(){
            Long counter = seed;
            @Override
            public Long get() {
                return counter++;
            }
        };
    }

    public static Supplier<Long> incrementThenGet(Long seed) {
        return new Supplier<Long>(){
            Long counter = seed;
            @Override
            public Long get() {
                return ++counter;
            }
        };
    }

    public static Function<Long, Long> makeAdder(Long seed) {
        return value -> seed + value;
    }
}
