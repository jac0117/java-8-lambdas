package conversant.extras;


import conversant.AdType;

public class SuperDuperExpensiveDBService {
    public PriceyAd fetchFor(AdType adType) {
        try {
            Thread.sleep(1000L);
            return new PriceyAd(adType,"Im expensive!");
        } catch (InterruptedException e) {

        }
        return null;
    }
}
