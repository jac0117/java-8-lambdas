package conversant.extras;

import java.util.Objects;

public class Money {
    public static final Money DE_NADA = new Money(0, "USD");

    public final Integer howMany;
    public final String units;

    public Money(Integer howMany, String units) {
        this.howMany = howMany;
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Money)) return false;
        Money money = (Money) o;
        return Objects.equals(howMany, money.howMany) &&
                Objects.equals(units, money.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(howMany, units);
    }

    public static Money plus(Money thiz, Money that) {
        return new Money(thiz.howMany + that.howMany, thiz.units);
    }

    @Override
    public String toString() {
        return "Money{" +
                "howMany=" + howMany +
                ", units='" + units + '\'' +
                '}';
    }
}
