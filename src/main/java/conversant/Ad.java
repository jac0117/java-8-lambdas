package conversant;

import java.util.Objects;

public class Ad {

    public AdType type;
    public String name;

    public Ad(AdType type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ad ad = (Ad) o;
        return Objects.equals(type, ad.type) &&
                Objects.equals(name, ad.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name);
    }
}
